# Scenario Visualizer #

This is the visual part for virus design project
There are 3 purposes now for the repo:

* compare the c++ version with python version
	- statistics: calculate the regression line
	- graph: show the curve
* Visual the whole process
* Give a framework for further research and data mining with logging file

## Validate the model
0. Data format:
	* {'cycle':{"e":num,"i":num,"r":num}}
1. epidemica curve
	* show the cycle and the infection numbers for each event type
	* show the infection number(y) with the cycle(x) with a curve
2. statistic tools
	* regression for the line
3. If the curve and statistic from python and c++ are similar, that should be valid model

## Visual the process
1. Visual Show all the data in the map
	* output the host data each cycle, and map it to the grid
2. How to fulfill that:
		* load the whole map with initial status
		* change it with the data, stop every cycle
3. The way to show it		

## Logging file framework
1. Visual Show every event happens:
	* infection in vectors E->I (Mosquito can only be dead, no R mode)
	* infection in hosts E->I/I->R
	* human movement
	* vector movement
	* host infection by vector S->E
	* vector infection by human S->E
	* vector mature (Dead and Born for Mosquito)
2. Records Now:
	1. cycle
	2. Event type (S->E => e, E->I => i, I->R => r)
	3. HostID
	4. LocationID

2. Logging file should include:
	* cycle
	* type: v:E->I,v:S->E,H:E->I,etc
	* location
	* host

## Environment Setup
* python2.7 for original code, which will be shown as ``python`` below
* python3.x for the visual and validate, which will be shown as ``python3`` below
* matplotlib: sudo apt-get install python-matplotlib
* numpy: ``pip3 install numpy``
* pandas: ``pip3 install pandas``
* of course, you will need pip3 first, for linux user: ``sudo apt-get install python3-pip``

## Run
1. Original: Add output log for the origianl python verision, the output is the same with cpp version
	- run ``python ArbovirusSimulator *.params``
	- the output wil be in **output.csv**
2. Validation: Validate the python and cpp version is similar, and also can be used to examine the results
	- **transfer.py** is used to clean the log data from cpp version
	- we need to delete the first line and the last line in log file from the server
	- run ``python3 validate.py`` and it will generate the graph as well as printing the formula
	- we can validate the model by comparing the graph with same data from cpp and python version
3. Visualization: Visualize the SEIR process to see what really happened
	- ``python3 visual.py``


# Further development

To Develop further, check with the Dev.md
