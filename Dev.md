# For Further development of Visualizer and Validation

## Structure
- Log file from cpp version and output from python version are generally in the same csv format
    - `[event,cycle,HostId,LocationID]`
    - cpp version will include the log time, which can be transferred by script `transfer.py`
    - events
        - e: S=>E
        - i: E=>I
        - r: I=>R  
    - The log includes the information about what, when, where, who for the event
- Process Framwork
    - Load data with `readcsv` function
    - Count the data about how many events happened in each cycle for each kind of event: `count_data`
    - To visualize it, load host on the map with initial status first, and when the event happened, make the host disappear from the previous state map, and show up in the next state.
- Further Development Part:
    - To generate or explore more from the data, change the way to deal with the data, choose some other statistic tools
