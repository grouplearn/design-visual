import json
import matplotlib
import pandas as pd
import numpy as np
import time
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
# Read csv file into data frame

filename = 'node0.csv'
locationfile = 'test1.locations'
hostfile = 'test1.hosts'
def readcsvfile(filename):
    data = pd.read_csv(filename,names = ["event", "cycle","HostID", "LocationID"])
    return data


def curve(data):
    # Curve
    E,I,R = count(data)

    draw_config(E,I,R)

# count the data with how many R/E/I events
def count(data):
    E = []
    I = []
    R = []
    count = data.groupby(['cycle','event']).size()
    for cycle in count.index.get_level_values('cycle').unique():
        # print(cycle)
        # print(count[cycle])
        for event in count[cycle].index:

            if event == 'e':
                E.append([cycle,count[cycle][event]])
            elif event == 'i':
                I.append([cycle,count[cycle][event]])
            else:
                R.append([cycle,count[cycle][event]])
    return E,I,R

# Plot the pic
def draw_config(E,I,R):
    draw_it(E,'E')
    draw_it(I,'I')
    draw_it(R,'R')

# Draw and save the png file
def draw_it(data,label):
    x = []
    y = []
    for k in data:
        x.append(k[0])
        y.append(k[1])
    line = np.polyfit(x,y,11)
    foruma = np.poly1d(line)
    print('python version equation:')
    print(foruma)
    yf = foruma(x)
    plt.figure()
    plt.plot(x,y,'*',label="Original Value")
    plt.plot(x,yf,'r',label="Polyfit Value")
    plt.xlabel('Cycle')
    plt.ylabel(label)
    plt.legend(loc=3)
    plt.ion()
    plt.savefig('python-'+label+'.png')
    plt.show()


def plot_it(data):


    # load the map
    hosts,locations = load_map_data(locationfile,hostfile)
    cycle_count = count_update(data)
    print(cycle_count)
    plot_basic_map(hosts,locations,cycle_count)






def count_update(data):
    data = data.sort_values(['cycle'], ascending=[True])
    cycle_count = []
    cycle = 0
    this_cycle = {'i':[],'r':[],'e':[]}
    for index, row in data.iterrows():
        if row['cycle'] == cycle:
            if row['event']=='i':
                this_cycle['i'].append(row['LocationID'])
            elif row['event'] == 'e':
                this_cycle['e'].append(row['LocationID'])
            elif row['event'] == 'r':
                this_cycle['r'].append(row['LocationID'])
        else:
            cycle_count.append(this_cycle)
            cycle += 1
            this_cycle = {'i':[],'r':[],'e':[]}
            if row['event']=='i':
                this_cycle['i'].append(row['LocationID'])
            elif row['event'] == 'e':
                this_cycle['e'].append(row['LocationID'])
            elif row['event'] == 'r':
                this_cycle['r'].append(row['LocationID'])
    # print(len(cycle_count))
    return cycle_count

def plot_basic_map(hosts,locations,cycle_count):
    Sx,Sy,Ex,Ey,Ix,Iy,Rx,Ry = [],[],[],[],[],[],[],[]
    for iid in hosts:
        state = hosts[iid]['i-state']
        location = hosts[iid]['location']
        coordinates = locations[hosts[iid]['location']]['coordinates']
        if state == 'S':
            Sx.append(coordinates[0])
            Sy.append(coordinates[1])
        elif state == 'E':
            Ex.append(coordinates[0])
            Ey.append(coordinates[1])
        elif state == 'I':
            Ix.append(coordinates[0])
            Iy.append(coordinates[1])
        else:
            Rx.append(coordinates[0])
            Ry.append(coordinates[1])
    plt.ion()
    fig,ax_list = plt.subplots(2,2)
    # plt.subplot(2,2,1)

    ax_list[0,0].scatter(Sx,Sy,color = 'blue',s=10,linewidths=0)
    ax_list[0,0].set_title('S')
    # plt.subplot(2,2,2)
    ax_list[0,1].scatter(Ex,Ey,color = "red",s=10,linewidths=0)
    # plt.subplot(2,2,3)
    ax_list[0,1].set_title('E')
    ax_list[1,0].scatter(Ix,Iy,color = 'purple',s=10,linewidths=0)
    # plt.subplot(2,2,4)
    ax_list[1,0].set_title('I')
    ax_list[1,1].scatter(Rx,Ry,color = 'grey',s=10,linewidths=0)
    # plt.savefig('basic.png')
    ax_list[1,1].set_title('R')
    plt.draw()

    cycle = cycle_count[0]

        # e: erease the first, show the second
    print(cycle['e'])
    Exupdate,Eyupdate = [],[]
    for points in cycle['e']:
        Exupdate.append(locations[points]['coordinates'][0])
        Eyupdate.append(locations[points]['coordinates'][1])
    ax_list[0,0].scatter(Exupdate,Eyupdate,color='white',s=10,linewidths=0)
    ax_list[0,1].scatter(Exupdate,Eyupdate,color="red",s=10,linewidths=0)
        # i: erease the second, show the third
        # print(cycle['i'])
    Ixupdate,Iyupdate = [],[]
    for points in cycle['i']:
        Ixupdate.append(locations[points]['coordinates'][0])
        Iyupdate.append(locations[points]['coordinates'][1])
        # print(Ixupdate,Iyupdate)
    ax_list[0,1].scatter(Ixupdate,Iyupdate,color='white',s=10,linewidths=0)
    ax_list[1,0].scatter(Ixupdate,Iyupdate,color="purple",s=10,linewidths=0)

        # r: erease the third, show the forth
        # print(cycle['r'])
    Rxupdate,Ryupdate = [],[]
    for points in cycle['r']:
        print(points)
        Rxupdate.append(locations[points]['coordinates'][0])
        Ryupdate.append(locations[points]['coordinates'][1])
    # print(cycle['r'])
    ax_list[1,0].scatter(Rxupdate,Ryupdate,color='white',s=10,linewidths=0)
    ax_list[1,1].scatter(Rxupdate,Ryupdate,color="grey",s=10,linewidths=0)
    plt.pause(1)

    for cycle in cycle_count:

        # e: erease the first, show the second
        print(cycle['e'])
        Exupdate,Eyupdate = [],[]
        for points in cycle['e']:
            Exupdate.append(locations[points]['coordinates'][0])
            Eyupdate.append(locations[points]['coordinates'][1])
        ax_list[0,0].scatter(Exupdate,Eyupdate,color='white',s=10,linewidths=0)
        ax_list[0,1].scatter(Exupdate,Eyupdate,color="red",s=10,linewidths=0)
        # i: erease the second, show the third
        # print(cycle['i'])
        Ixupdate,Iyupdate = [],[]
        for points in cycle['i']:
            Ixupdate.append(locations[points]['coordinates'][0])
            Iyupdate.append(locations[points]['coordinates'][1])
        # print(Ixupdate,Iyupdate)
        ax_list[0,1].scatter(Ixupdate,Iyupdate,color='white',s=10,linewidths=0)
        ax_list[1,0].scatter(Ixupdate,Iyupdate,color="purple",s=10,linewidths=0)

        # r: erease the third, show the forth
        # print(cycle['r'])
        Rxupdate,Ryupdate = [],[]
        for points in cycle['r']:
            Rxupdate.append(locations[points]['coordinates'][0])
            Ryupdate.append(locations[points]['coordinates'][1])
        # print(cycle['r'])
        ax_list[1,0].scatter(Rxupdate,Ryupdate,color='white',s=10,linewidths=0)
        ax_list[1,1].scatter(Rxupdate,Ryupdate,color="grey",s=10,linewidths=0)
        plt.pause(0.01)



    return Sx,Sy,Ex,Ey,Ix,Iy,Rx,Ry


def load_map_data(location_file,host_file):
    locations = {}
    hosts = {}
    with open(location_file,'r') as f:
        locData = json.load(f)
    for cids in locData:
        cid            = int(cids)
        c              = locData[cids]
        c['cid']       = cid

        # build in function, a set of params
        c['hosts']     = set()
        locations[cid] = c

    # print(locations)
    with open(host_file,'r') as f:
        hostData = json.load(f)
    for iids in hostData:
        iid        = int(iids)
        h          = hostData[iids]
        h['iid']   = iid
        hosts[iid] = h
        locations[h['location']]['hosts'].add(iid)
    # print(hosts)
    return hosts,locations



def dynamic_visual():
    #data = readcsvfile('output.csv')
    data = readcsvfile(filename)
    # Dynamic Plot
    plot_it(data)




dynamic_visual()
