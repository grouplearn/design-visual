import csv

filename = "node0.log"
def main():
    with open(filename) as f:
        content = f.readlines()
    content = [x.strip().split('[default]')[1].replace(" ", "").split(',') for x in content] 

    print(content)
    with open("node0.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerows(content)
    return False
main()